Source: dbus
Section: admin
Priority: optional
Maintainer: Devuan Maintainers <devuan-dev@lists.dyne.org>
Uploaders: Denis Roio <jaromil@dyne.org>,
    Vincenzo (KatolaZ) Nicosia <katolaz@freaknet.org>,
    Daniel Reurich <daniel@centurion.net.nz>,
    Lorenzo "Palinuro" Faletra <palinuro@parrotsec.org>
# The following packages can be omitted for bootstrapping, but provide extra
# features:
#   libapparmor-dev
#   libaudit-dev
#   libcap-ng-dev
# The following packages can be omitted for bootstrapping, but provide extra
# debugging support in /usr/lib/*/dbus-1.0/debug-build:
#   valgrind
# The following packages can be omitted for bootstrapping, but improve test
# coverage:
#   libglib2.0-dev
#   python
#   python-dbus (circular dependency)
#   python-gobject
Build-Depends: 
 autoconf-archive (>= 20150224),
 automake,
 debhelper (>= 11.1~),
 dh-exec,
 libapparmor-dev [linux-any] <!pkg.dbus.minimal>,
 libaudit-dev [linux-any] <!pkg.dbus.minimal>,
 libcap-ng-dev [linux-any] <!pkg.dbus.minimal>,
 libexpat-dev,
 libglib2.0-dev <!pkg.dbus.minimal>,
 libnss-wrapper <!nocheck>,
 libselinux1-dev [linux-any] <!pkg.dbus.minimal>,
 libx11-dev <!pkg.dbus.minimal>,
 python3 <!nocheck !pkg.dbus.minimal>,
 python3-dbus <!nocheck !pkg.dbus.minimal>,
 python3-gi <!nocheck !pkg.dbus.minimal>,
 valgrind [amd64 arm64 armhf i386 mips64 mips64el mips mipsel powerpc ppc64 ppc64el s390x] <!pkg.dbus.minimal>,
 xmlto <!nodoc !pkg.dbus.minimal>,
Build-Depends-Indep:
 doxygen <!nodoc>,
 ducktype <!nodoc>,
 xsltproc <!nodoc>,
 yelp-tools <!nodoc>,
Standards-Version: 4.2.1
Vcs-Git: https://git.devuan.org/packages-base/dbus.git
Vcs-Browser: https://git.devuan.org/packages-base/dbus
Homepage: http://dbus.freedesktop.org/
Rules-Requires-Root: no

Package: dbus
Architecture: any
Priority: standard
Provides:
 dbus-bin (= ${binary:Version}),
 dbus-system-bus (= ${binary:Version}),
Depends:
 adduser,
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 default-dbus-session-bus | dbus-session-bus,
Multi-Arch: foreign
Description: simple interprocess messaging system (daemon and utilities)
 D-Bus is a message bus, used for sending messages between applications.
 Conceptually, it fits somewhere in between raw sockets and CORBA in
 terms of complexity.
 .
 D-Bus supports broadcast messages, asynchronous messages (thus
 decreasing latency), authentication, and more. It is designed to be
 low-overhead; messages are sent using a binary protocol, not using
 XML. D-Bus also supports a method call mapping for its messages, but
 it is not required; this makes using the system quite simple.
 .
 It comes with several bindings, including GLib, Python, Qt and Java.
 .
 This package contains the D-Bus daemon and related utilities.
 .
 The client-side library can be found in the libdbus-1-3 package, as it is no
 longer contained in this package.

Package: dbus-1-doc
Build-Profiles: <!nodoc !pkg.dbus.minimal>
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Suggests:
 libdbus-1-dev,
Description: simple interprocess messaging system (documentation)
 D-Bus is a message bus, used for sending messages between applications.
 Conceptually, it fits somewhere in between raw sockets and CORBA in
 terms of complexity.
 .
 This package contains the API documentation for D-Bus, as well as
 the protocol specification.
 .
 See the dbus description for more information about D-Bus in general.

Package: dbus-tests
Build-Profiles: <!pkg.dbus.minimal>
Section: misc
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 dbus,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 gnome-desktop-testing,
Breaks:
 dbus-1-dbg (<< 1.11.10-2~),
Replaces:
 dbus-1-dbg (<< 1.11.10-2~),
Description: simple interprocess messaging system (test infrastructure)
 D-Bus is a message bus, used for sending messages between applications.
 Conceptually, it fits somewhere in between raw sockets and CORBA in
 terms of complexity.
 .
 This package provides automated and manual tests for D-Bus, and the
 dbus-test-tool utility. It also provides copies of the D-Bus libraries and
 executables compiled with extra debug information and logging.
 .
 See the dbus package description for more information about D-Bus in general.

Package: dbus-udeb
Build-Profiles: <!noudeb>
Section: debian-installer
Package-Type: udeb
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: simple interprocess messaging system (minimal runtime)
 D-Bus is a message bus, used for sending messages between applications.
 .
 This package is a minimal version of the dbus and dbus-x11 packages,
 for use in the Debian installer. It can run a session bus, but is not
 suitable for use as a system bus.

Package: dbus-x11
Build-Profiles: <!pkg.dbus.minimal>
Architecture: any
Section: x11
Provides:
 dbus-session-bus,
 default-dbus-session-bus [!linux-any],
Depends:
 dbus,
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: foreign
Description: simple interprocess messaging system (X11 deps)
 D-Bus is a message bus, used for sending messages between applications.
 Conceptually, it fits somewhere in between raw sockets and CORBA in
 terms of complexity.
 .
 This package contains the dbus-launch utility which is necessary for
 packages using a D-Bus session bus.
 .
 See the dbus description for more information about D-Bus in general.

Package: libdbus-1-3
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 dbus (<< 1.9.16-1~),
Recommends:
 dbus,
Section: libs
Description: simple interprocess messaging system (library)
 D-Bus is a message bus, used for sending messages between applications.
 Conceptually, it fits somewhere in between raw sockets and CORBA in
 terms of complexity.
 .
 D-Bus supports broadcast messages, asynchronous messages (thus
 decreasing latency), authentication, and more. It is designed to be
 low-overhead; messages are sent using a binary protocol, not using
 XML. D-Bus also supports a method call mapping for its messages, but
 it is not required; this makes using the system quite simple.
 .
 It comes with several bindings, including GLib, Python, Qt and Java.
 .
 The daemon can be found in the dbus package.

Package: libdbus-1-3-udeb
Build-Profiles: <!noudeb>
Section: debian-installer
Package-Type: udeb
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: simple interprocess messaging system (minimal library)
 D-Bus is a message bus, used for sending messages between applications.
 .
 This package is a minimal version of the libdbus-1-3 package,
 for use in the Debian installer.

Package: libdbus-1-dev
Section: libdevel
Multi-Arch: same
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 libdbus-1-3 (= ${binary:Version}),
 pkg-config,
 ${misc:Depends},
 ${shlibs:Depends},
Description: simple interprocess messaging system (development headers)
 D-Bus is a message bus, used for sending messages between applications.
 Conceptually, it fits somewhere in between raw sockets and CORBA in
 terms of complexity.
 .
 See the dbus description for more information about D-Bus in general.
